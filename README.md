# Wardrobify

Team:

* Danielle Chang - Hat
* Leon Moun - Shoes

## Design

## Shoes microservice

Features
View a list of available shoes in the store.
Click on a shoe to view its details.
Each shoe displays its name, manufacturer, color, and an optional picture.
Navigate to different shoes using the provided links.

## Hats microservice

The Hat Model is represented by the fabric, style, color, picture_url, and location. The Location Model in the hat api is correlated to the Location Model in the wardrobe api by its value objects which include the closet_name, section_number, and shelf_number.
