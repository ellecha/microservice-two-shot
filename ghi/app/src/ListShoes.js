import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './ListShoes.css'; // Import the CSS file with the correct path

const ListShoes = () => {
  const [shoes, setShoes] = useState([]);

  const fetchShoes = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/shoes/');
      const data = await response.json();
      setShoes(data.shoes);
    } catch (error) {
      console.error('Error fetching shoes:', error);
    }
  };

  useEffect(() => {
    fetchShoes();
  }, []);

  return (
    <div>
      <h2>Our Shoes!</h2>
      <div className="shoe-list">
        {shoes.map((shoe) => (
          <div key={shoe.id} className="shoe-card">
            {shoe.picture_url && (
              <img
                src={shoe.picture_url}
                alt={shoe.name}
                className="shoe-image"
              />
            )}
            <div className="shoe-details">
              <p>{shoe.name}</p>
              <p>{shoe.manufacturer}</p>
              <p>{shoe.color}</p>
              <Link to={`/shoes/${shoe.id}`}>View Details</Link>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ListShoes;
