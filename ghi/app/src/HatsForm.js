import React, { useEffect, useState } from "react";

function HatsForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState({
    fabric: "",
    color: "",
    style: "",
    picture_url: "",
    location: "",
  });

  const fetchData = async () => {
    const locationUrl = "http://localhost:8100/api/locations/";
    const response = await fetch(locationUrl);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []); // Added an empty dependency array to useEffect, to ensure it runs only once on component mount.

  const handleSubmit = async (event) => {
    event.preventDefault();

    const hatUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        fabric: "",
        color: "",
        style: "",
        picture_url: "",
        location: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
                value={formData.fabric}
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
                value={formData.color}
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Style"
                required
                type="text"
                name="style"
                id="style"
                className="form-control"
                value={formData.style}
              />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Picture"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
                value={formData.picture_url}
              />
              <label htmlFor="picture_url">Picture</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                required
                name="location"
                id="location"
                className="form-select"
                value={formData.location}
              >
                <option value="">Choose a location</option>
                {locations.map((location) => (
                  <option key={location.id} value={location.id}>
                    {location.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatsForm;
