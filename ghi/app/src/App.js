import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListShoes from './ListShoes';
import CreateShoe from './CreateShoe';
import ShoeDetail from './ShoeDetail';
import HatsForm from './HatsForm'
import HatsList from './HatsList'

function App() {
  return (
    <Router>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ListShoes />} />
          <Route path="/create" element={<CreateShoe />} />
          <Route path="/shoes/:id" element={<ShoeDetail />} />
          <Route path= "hats">
            <Route path="all" element={<HatsList />} />
            <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
