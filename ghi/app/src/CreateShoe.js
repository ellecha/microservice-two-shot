import React, { useState } from 'react';

const CreateShoe = () => {
  const [formData, setFormData] = useState({
    name: '',
    manufacturer: '',
    color: '',
    picture_url: '',
  });
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/shoes/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
    } catch (error) {
      console.error('Error creating shoe:', error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  return (
    <div>
      <h2>Create a New Shoe</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input type="text" name="name" value={formData.name} onChange={handleChange} />
        </label>
        <label>
          Manufacturer:
          <input type="text" name="manufacturer" value={formData.manufacturer} onChange={handleChange} />
        </label>
        <label>
          Color:
          <input type="text" name="color" value={formData.color} onChange={handleChange} />
        </label>
        <label>
          Picture URL:
          <input type="text" name="picture_url" value={formData.picture_url} onChange={handleChange} />
        </label>
        <button type="submit">Create Shoe</button>
      </form>
    </div>
      );
    };

export default CreateShoe;
