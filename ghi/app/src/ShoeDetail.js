import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

const ShoeDetail = ({ handleDelete }) => {
  const [shoe, setShoe] = useState(null);
  const { id } = useParams();

  const fetchShoeDetails = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}/`);
      const data = await response.json();
      setShoe(data);
    } catch (error) {
      console.error('Error fetching shoe details:', error);
    }
  };

  useEffect(() => {
    fetchShoeDetails();
  }, [id]);


  const onDeleteClick = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, {
        method: 'DELETE',
      });

      if (response.ok) {
        handleDelete();
      } else {
        console.error('Failed to delete shoe.');
      }
    } catch (error) {
      console.error('Error deleting shoe:', error);
    }
  };

  if (!shoe) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2>Shoe Details</h2>
      <p>Name: {shoe.name}</p>
      <p>Manufacturer: {shoe.manufacturer}</p>
      <p>Color: {shoe.color}</p>
      {shoe.picture_url && <img src={shoe.picture_url} alt={shoe.name} style={{ maxWidth: '300px' }} />}
      <button onClick={onDeleteClick}>Delete</button>
    </div>
  );
};

export default ShoeDetail;
