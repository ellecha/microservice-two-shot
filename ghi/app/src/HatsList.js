import React from "react";

function HatsList(props) {
    return (
    <div>
        <h2>Our Hats</h2>
      <div className="hat-list">
          {props.hats.map((hat) => (
            <div key={hat.id} className="hat-card">
              {hat.picture_url && (
              <img
                src={hat.picture_url}
                alt={hat.name}
                className="hat-image"
                />
              )}
              <div className="hat-details">
                <p>{hat.name}</p>
                <p>{hat.style}</p>
                <p>{hat.color}</p>
                </div>
              </div>
          ))}
        </div>
      </div>
    );
  }

  export default HatsList;
