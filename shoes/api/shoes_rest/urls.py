from django.urls import path
from . import views

urlpatterns = [
    path('shoes/', views.api_list_shoes, name='shoe-list-create'),
    path('shoes/<int:pk>/', views.api_show_delete_shoe, name='shoe-show-delete'),
]
