from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Shoe, BinVO
from common.json import ModelEncoder

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]
    
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    
    encoders = {
        "bin": BinVOEncoder(),
    }
    
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "picture_url",
        "name",
        "manufacturer",
        "color",
        "id",
    ]
        
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        
        try: 
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID!"},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
        
@require_http_methods(["GET", "DELETE"])
def api_show_delete_shoe(request, pk):
    try:
        shoe = Shoe.objects.get(id=pk)
    except Shoe.DoesNotExist:
        return JsonResponse({"message": "Shoe does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        shoe.delete()
        return JsonResponse(
            {"message": "Shoe deleted successfully"},
            status=200,
        )       
